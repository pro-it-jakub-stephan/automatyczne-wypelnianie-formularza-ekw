// ==UserScript==
// @name         Alvin - wypełnij numer ksiegi automatycznie
// @namespace    PRO IT Jakub Stephan
// @version      1.1.3
// @description  try to take over the world!
// @homepageURL  https://gitlab.com/pro-it-jakub-stephan/automatyczne-wypelnianie-formularza-ekw
// @supportURL   https://gitlab.com/pro-it-jakub-stephan/automatyczne-wypelnianie-formularza-ekw/-/issues
// @updateURL    https://gitlab.com/pro-it-jakub-stephan/automatyczne-wypelnianie-formularza-ekw/-/raw/main/script.user.js
// @downloadURL  https://gitlab.com/pro-it-jakub-stephan/automatyczne-wypelnianie-formularza-ekw/-/raw/main/script.user.js
// @history For a history of changes, see CHANGELOG.md in the source repository.
//
// @grant none
// @author       You
// @match        https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gov.pl
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    // Find page elements first
    const court_element = document.getElementById('kodWydzialuInput');
    const number_element = document.getElementById('numerKsiegiWieczystej');
    const control_element = document.getElementById('cyfraKontrolna');

    // Check if URL contains # and inputs are empty
    if (window.location.href.includes('#') && !court_element.value && !number_element.value && !control_element.value) {
        // Get number from URL
        const land_register = window.location.href.split('#')[1]
        // Split number into small pieces
        let [court, number, control] = land_register.split('/')
        // Fill in inputs
        // TODO: Known issue that court number needs to be selected again manually
        court_element.value = court;
        while (number.length < 8) {
            number = '0' + number
        }
        number_element.value = number;
        control_element.value = control;
    } else {
        console.log('Skip processing');
    }
})();