# Automatyczne wypełnianie formularza EKW

## O projekcie

Projekt typu `user-script` który na określonej podstronie, po spełnieniu wymagań uzupełni automatycznie wartości pól formularza zgodnie z przesłanymi w adresie URL parametrami.

Numer powinien znajdować się w adresie po znaku `#`.